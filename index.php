
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9">
<![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Ktm Immigration Pvt Ltd | </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="Author" content="DBASoftTech"/>
    <meta property="og:image" content="https://ktmimmigration.com.np/frontend/default/img/logo.png">
    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <link rel="shortcut icon" href="https://ktmimmigration.com.np/frontend/default/images/logo.ico" type="image/x-icon">
    <!-- CSS -->
        <link rel="stylesheet" href="https://ktmimmigration.com.np/frontend/default/css/master.css">
    <link rel="stylesheet" href="https://ktmimmigration.com.np/frontend/default/css/unslider.css">
    <link rel="stylesheet" href="https://ktmimmigration.com.np/frontend/default/css/unslider-dots.css">
    <link rel="stylesheet" href="https://ktmimmigration.com.np/frontend/default/css/animate.css">
    <link rel="stylesheet"
          href="https://ktmimmigration.com.np/sximo/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://ktmimmigration.com.np/sximo/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css">

        <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set._.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.src = "https://v2.zopim.com/?4awRyR0Egoq8kI84hJQvRqjpgmm1eK6N";
            z.t = +new Date;
            $.type = "text/javascript";
            e.parentNode.insertBefore($, e)
        })(document, "script");
        $zopim(function () {
            $zopim.livechat.button.show();
        });
    </script>

    <style type="text/css">
        .zopim {
            display: none;
        }

        #at-custom-sidebar {
            top: 35% !important;
        }

        .demo1 {
            height: 398px !important;
            /*overflow-y: scroll !important;*/
            overflow: hidden;
        }

        .demo1 td {
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--bookanappointment-->
<div id="bookanappointment">
    <a href="https://ktmimmigration.com.np/bookAppointment"><img src="https://ktmimmigration.com.np/frontend/default/img/book_appointment.png"/></a>
</div>
<!--//bookanappointment-->
<!--livechat-->
<div id="livechat">
    <a href="#" id="techSupport"><img src="https://ktmimmigration.com.np/frontend/default/img/ask-the-expert-icon.png"/></a>
    <a href="#" id="askTheExpertClose">X</a>
</div>
<!-- navigation -->
<header id="this-is-top">
    <div class="container">
        <div class="topmenu row">
            <nav id="topNav"
                 class="alll-too blockDisplay col-sm-offset-3 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-6 col-lg-6">
                <ul>
                                        <li>
                        <a href="#">Branches &#9662;</a>
                        <ul class="dropdown">
                                                            <li><a href="#">KTM IMMIGRATION</a></li>
                                                            <li><a href="#">Advance Immigration</a></li>
                                                    </ul>
                    </li>
                    <li><a href="https://ktmimmigration.com.np/downloads">Download</a></li>
                    <li><a href="https://ktmimmigration.com.np/dispPages/why-us">Why Us?</a></li>
                                            <li><a href="https://ktmimmigration.com.np/studentLogin">Sign In</a></li>
                        <li><a href="https://ktmimmigration.com.np/signUp">Sign Up</a></li>
                                    </ul>
            </nav>

            <nav class="text-right col-sm-3 col-md-2 col-lg-2">
                <a class="social-icons" target="_blank" href="https://www.facebook.com/KTMIMMIGRATION/"><i
                            class="fa fa-facebook"></i></a>
                <a class="social-icons" target="_blank" href="https://plus.google.com/u/1/108551602522247848911"><i
                            class="fa fa-google-plus"></i></a>
                <a class="social-icons" target="_blank" href="https://twitter.com/KtmImmigration"><i
                            class="fa fa-twitter"></i></a>
                <a class="social-icons" target="_blank"
                   href="https://www.youtube.com/channel/UCRZ-WtDYA9WcT26LzHM3-TQ"><i class="fa fa-youtube"></i></a>
            </nav>
        </div>
        <div class="row header">
            <div class="col-sm-3 col-md-3 col-lg-3">
                <a href="https://ktmimmigration.com.np" id="logo"></a>
            </div>
            <div class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-8 col-md-8 col-lg-8">
                <div class="text-right header-padding" id="contactInfo">
                    <div class="h-block"><span>CALL US</span>+977-01-4426263</div>
                    <div class="h-block"><span>EMAIL US</span>immigrationktm@gmail.com</div>
                    <div class="h-block"><span>WORKING HOURS</span>Sun - Fri 10.00 - 5.00</div>
                    <a class="btn btn-success" href="https://ktmimmigration.com.np/applyOnlineNow">Apply Online Now !</a>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar-default navbar arse affix" data-spy="affix" data-offset-top="150">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
            <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse" aria-expanded="false">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="https://ktmimmigration.com.np">HOME</a>
                    </li>
                    <li>
                        <a href="https://ktmimmigration.com.np/dispPages/about-us">ABOUT US</a>
                    </li>
                    <li>
                        <a href="https://ktmimmigration.com.np/allservices">SERVICES</a>
                    </li>
                    <li><a href="https://ktmimmigration.com.np/countriesList">COUNTRY INFORMATION</a></li>
                    <li><a href="https://ktmimmigration.com.np/universityList">UNIVERSITY INFORMATION</a></li>
                    <li><a href="https://ktmimmigration.com.np/blog">Blog</a></li>
                    <li><a class="btn_header_search" href="#"><i class="fa fa-search"></i></a></li>
                </ul>
            </div>
        </div>


    </nav>
    <div class="search-form-modal transition">
        <form method="GET" action="https://ktmimmigration.com.np/topSearchDetails" accept-charset="UTF-8" class="navbar-form header_search_form" role="search" autocomplete="on">
        <i class="fa fa-times search-form_close"></i>
        <div class="form-group">
            <input type="text" id="keywordsearch" name="keyword" class="form-control"
                   placeholder="Search">
        </div>
        <button type="submit" id="click_top_search" class="btn btn_search customBgColor">Search</button>
        </form>
    </div>


</header>
<!-- //navigation -->
<!-- slider -->
<div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-main-slider="true" data-pagination="false" data-single-item="true" data-stop-on-hover="true" id="owl-main-slider">
        <div class="item">
        <img alt="Img" src="https://ktmimmigration.com.np/uploads/slideshow/1488870539-68631350.jpg">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a class="next">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>
                                STUDY IN AUSTRALIA
                            </h1>
                        </div>
                    </div>
                    <p>
                        Australia, known as the land of Kangaroos has attracted the third largest number of the international students to come and study in Australia. Every year around 400,000 overseas students get educated from Australian universities, colleges and institutes.
                    </p>
                    <a class="btn btn-success" href="http://ktmimmigration.edu.np/singleCountryDetails/MQ==">
                        LEARN MORE
                    </a>
                </div>
            </div>
        </img>
    </div>
        <div class="item">
        <img alt="Img" src="https://ktmimmigration.com.np/uploads/slideshow/1492327102-64669873.jpg">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a class="next">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>
                                Study in USA
                            </h1>
                        </div>
                    </div>
                    <p>
                        USA, a highly developed country, which has a very advanced education system that varies greatly across the country. 
                    </p>
                    <a class="btn btn-success" href="http://ktmimmigration.edu.np/singleCountryDetails/NQ==">
                        LEARN MORE
                    </a>
                </div>
            </div>
        </img>
    </div>
        <div class="item">
        <img alt="Img" src="https://ktmimmigration.com.np/uploads/slideshow/1492326921-75802302.jpg">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a class="next">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>
                                Study In Japan
                            </h1>
                        </div>
                    </div>
                    <p>
                        Japan is a group of Island country having the world's 3rd largest economy.Around 75% of the total international students works there as the unemployment rate is only 4%.
                    </p>
                    <a class="btn btn-success" href="http://ktmimmigration.edu.np/singleCountryDetails/Mg==">
                        LEARN MORE
                    </a>
                </div>
            </div>
        </img>
    </div>
        <div class="item">
        <img alt="Img" src="https://ktmimmigration.com.np/uploads/slideshow/1492329110-30693568.jpg">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a class="next">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>
                                STUDY IN NEW ZEALAND
                            </h1>
                        </div>
                    </div>
                    <p>
                        New Zealand is an attractive destination for international students and the country’s educational institutions are well regarded throughout the world.
                    </p>
                    <a class="btn btn-success" href="http://ktmimmigration.edu.np/singleCountryDetails/Nw==">
                        LEARN MORE
                    </a>
                </div>
            </div>
        </img>
    </div>
        <div class="item">
        <img alt="Img" src="https://ktmimmigration.com.np/uploads/slideshow/1492329877-63265030.jpg">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a class="next">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>
                                STUDY IN NORWAY
                            </h1>
                        </div>
                    </div>
                    <p>
                        Everything about norway
                    </p>
                    <a class="btn btn-success" href="">
                        LEARN MORE
                    </a>
                </div>
            </div>
        </img>
    </div>
    </div>
<!-- //slider -->
<div class="container block-content1 marg_25">
    <div class="row">
        <div class="col-sm-5 col-md-5 col-lg-5 " >
            <!--destination_explore-->
            <div class="searchDestination">
                <h1 class="color-1 cust_marg">
                    Explore Your Study Options
                </h1>

                <form action="https://ktmimmigration.com.np/searchDetails" method="get">
                    <select class="form-control" id="validation" name="searchDestination">
                        <option selected value="0">
                            Country
                        </option>
                                                    <option value="Nepal"> Nepal</option>
                        </option>
                                                    <option value="Nepal"> Nepal</option>
                        </option>
                                            </select>
                    <select class="form-control" id="validation" name="levelofcourse">
                        <option selected="" value="">
                            Level of Course you're looking for
                        </option>
                                                <option style="text-transform: capitalize;" value="Level of Course you&#039;re looking for">
                            Level of Course you&#039;re looking for
                        </option>
                                                <option style="text-transform: capitalize;" value="Under Diploma">
                            Under Diploma
                        </option>
                                                <option style="text-transform: capitalize;" value="Diploma/ advance diploma ">
                            Diploma/ advance diploma 
                        </option>
                                                <option style="text-transform: capitalize;" value="Undergraduate">
                            Undergraduate
                        </option>
                                                <option style="text-transform: capitalize;" value="Postgraduate">
                            Postgraduate
                        </option>
                                                <option style="text-transform: capitalize;" value="Masters">
                            Masters
                        </option>
                                                <option style="text-transform: capitalize;" value="Language Class">
                            Language Class
                        </option>
                                            </select>
                    <select class="form-control" id="validation" name="areaofinterest">
                        <option selected="" value="">
                            Your area of Interest
                        </option>
                        
                        <option style="text-transform: capitalize;" value="Maths">
                            Maths
                        </option>
                        
                        <option style="text-transform: capitalize;" value="Science">
                            Science
                        </option>
                        
                        <option style="text-transform: capitalize;" value="Commerce">
                            Commerce
                        </option>
                        
                        <option style="text-transform: capitalize;" value="5458252929">
                            5458252929
                        </option>
                                            </select>
                    <button class="btn btn-danger" id="click_btn" type="submit">
                        SEARCH
                    </button>
                    <div class="error-msg">
                    </div>
                </form>
            </div>
            <!--//destination_explore-->
        </div>
        <div class="col-sm-7 col-md-7 col-lg-7 " >
            <div class="quote-form">
                <h1 class="color-1 cust_marg" style="margin-bottom:10px;">
                    welcome to ktmimmigration
                </h1>
                <p>
                    Preparing for IELTS exam ? Yet in dilemma whether you are qualified or not ? Test Yourself !
                </p>
                <a class="btn btn-success cust-btn" href="https://ktmimmigration.com.np/bookMockTest" style="width:200px; margin-bottom:10px;">
                    Book Your Mock Test
                </a>
                <br>
                    <h1 class="color-1 cust_marg" style="margin-bottom:10px;">
                        BOOK YOUR IELTS DATE
                    </h1>
                    <p>
                        Do you want to book a date for IELTS exam? Send us all required documents and we will book IELTS
                        date on your behalf.
                    </p>
                    <a class="btn btn-success cust-btn" href="https://ktmimmigration.com.np/ieltsBookingForm" style="width:150px;">
                        Book
                        Now
                    </a>
                </br>
            </div>
        </div>
    </div>
</div>
<div class="block-content inner-offset">
    <div class="info-texts " >
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4" style="height:120px;">
                    <a class="btn btn-success" href="https://ktmimmigration.com.np/bookIeltsClass" style="z-index: 1000;
    background-color: #eb1a20;">
                        Book Ielts Class
                    </a>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4" style="height:150px;">
                    <a class="btn btn-success" href="https://ktmimmigration.com.np/downloads">
                        Visit Learning Centre
                    </a>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <p>
                        We provide Test Preparation services (IELTS, TOEFL, GRE, GMAT), Abroad Study Counselling and
                            Consultancy Services
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row column-info">
                <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
            <img alt="Img" src="https://ktmimmigration.com.np/uploads/countries/1492329979-16583009.jpg">
                <span>
                </span>
                <h3>
                    STUDY IN AUSTRALIA
                </h3>
                <p class="countrySummary">
                    Australia,
known as the land of Kangaroos has attracted the third largest number of the
international students to come and study in Australia. Every...
                </p>
                <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/singleCountryDetails/MQ==">
                    READ MORE
                </a>
            </img>
        </div>
                <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
            <img alt="Img" src="https://ktmimmigration.com.np/uploads/countries/1492327555-26196619.jpg">
                <span>
                </span>
                <h3>
                    STUDY IN JAPAN
                </h3>
                <p class="countrySummary">
                    Japan is a group of Island country having the world's 3rd largest economy.Around 75% of the total international students works there as the unemployme...
                </p>
                <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/singleCountryDetails/Mg==">
                    READ MORE
                </a>
            </img>
        </div>
                <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
            <img alt="Img" src="https://ktmimmigration.com.np/uploads/countries/1492328740-534336.jpg">
                <span>
                </span>
                <h3>
                    STUDY IN USA
                </h3>
                <p class="countrySummary">
                    USA, a highly developed
country, which has a very advanced education
system that varies greatly across the country. 
                </p>
                <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/singleCountryDetails/NQ==">
                    READ MORE
                </a>
            </img>
        </div>
                <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
            <img alt="Img" src="https://ktmimmigration.com.np/uploads/countries/1492328876-59802272.jpg">
                <span>
                </span>
                <h3>
                    STUDY IN NEW ZEALAND
                </h3>
                <p class="countrySummary">
                    New Zealand is an attractive destination for
international students and the country’s educational institutions are well
regarded throughout the worl...
                </p>
                <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/singleCountryDetails/Nw==">
                    READ MORE
                </a>
            </img>
        </div>
                <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
            <img alt="Img" src="https://ktmimmigration.com.np/uploads/countries/1492329834-69036381.jpg">
                <span>
                </span>
                <h3>
                    STUDY IN NORWAY
                </h3>
                <p class="countrySummary">
                    Norway, with its population of just over 5 million, is ranked as one of the best countries to live in and has one of the lowest crime rates in the wor...
                </p>
                <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/singleCountryDetails/OQ==">
                    READ MORE
                </a>
            </img>
        </div>
            </div>
</div>
<div class="big-hred">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="ribbon">
                    <strong class="ribbon-content">
                        <div class="text-center" style="margin-right:40px;">
                            <h2>
                                WE PROVIDE QUALITY COUNSELLING AND CONSULTANT SERVICES
                            </h2>
                            <p>
                                One of our highly trained counsellors will review your documents and will be in touch to
                                    discuss your study goals.
                            </p>
                        </div>
                    </strong>
                    <div class="row-content">
                        <a class="btn btn-danger btn-lg" href="https://ktmimmigration.com.np/applyOnlineNow">
                            APPLY ONLINE NOW
                        </a>
                    </div>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="wrapper-testpreparation">
    <div class="container block-content">
        <div class="hgroup text-center " >
            <h1 id="testPreparation">
                TEST PREPARATION
            </h1>
            <h2>
                We provide preparation class services for various online tests as following.
            </h2>
        </div>



        <div class="row column-info" id="testPreparationContent">
                      <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
                <img src="https://ktmimmigration.com.np/uploads/testpreparation/1495368326-79248255.png" style="float: left; margin: 3px 9px;  max-width: 111px;"/>
                <p>
                    The International English Language Testing System (IELTS) is an internationally-owned and globally-recognized direct English language assessment of the utmost quality and integrity, readily available throughout the world
                </p>
                <a class="btn btn-default btn-sm" href="  https://ktmimmigration.com.np/preparationdetails/MQ==">
                    READ MORE
                </a>
            </div>
                        <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
                <img src="https://ktmimmigration.com.np/uploads/testpreparation/1495381993-54138642.png" style="float: left; margin: 3px 9px;  max-width: 111px;"/>
                <p>
                    Your SAT score is a pivotal component of your college applications. Most colleges use these scores to help decide whether to admit students or not.
                </p>
                <a class="btn btn-default btn-sm" href="  https://ktmimmigration.com.np/preparationdetails/Mg==">
                    READ MORE
                </a>
            </div>
                        <div class="col-sm-4 col-md-4 col-lg-4  marg_25" >
                <img src="https://ktmimmigration.com.np/uploads/testpreparation/1495383617-51074708.png" style="float: left; margin: 3px 9px;  max-width: 111px;"/>
                <p>
                    TOEFL is a standardized test to measure the English language ability of non-native speakers wishing to enroll in universities.
                </p>
                <a class="btn btn-default btn-sm" href="  https://ktmimmigration.com.np/preparationdetails/Mw==">
                    READ MORE
                </a>
            </div>
                    </div>
    </div>
</div>
<div class="container block-content">
    <div class="row">
        <div class="col-md-4 col-lg-4 " >
            <div class="hgroup">
                <h1>
                    Student Testimonials
                </h1>
            </div>
            <div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-navigation="true" data-pagination="false" data-single-item="true" id="testimonials">
                                <div>
                    <div class="testimonial-content" style="padding:10px;">
                            <img class="img-responsive" height="65" src="https://ktmimmigration.com.np/uploads/testimonialImg/1502848492-90066315.jpg" width="65">
                            </img>
                      
                       
                            <p>
                                <p>I believe IELTS band score plays a significant role in an abroad studies application process hence i would recommend a careful consideration while choosing the preparation center. And KTM IMMIGRATION is no doubt one of the best places to gear your test preparation as the flexibility of class timings, the friendly classroom decorum  surely enables one to interact more with the instructor and among fellow classmates.</p>
                            </p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>
                            Asmita K.C.
                        </h4>
                        <small>
                            K.T.M Immigration, Putalisadak, Kathmandu
                        </small>
                    </div>
                </div>
                                <div>
                    <div class="testimonial-content" style="padding:10px;">
                            <img class="img-responsive" height="65" src="https://ktmimmigration.com.np/uploads/testimonialImg/1502850427-75719872.jpg" width="65">
                            </img>
                      
                       
                            <p>
                                <p class="MsoNormal"><span style="font-size: 11px;">It was a very good time for me being here at Ktm Immigration for couple of month. Luckily I went to this institution where I found co-operative members and versatile instructor. The technology here made our learning environment worth it. Honestly speaking, this institution had been my best Platform for my further day education as it gave me best guidelines.</span><span style="font-size: 16pt;"><o:p></o:p></span></p>
                            </p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>
                            Amardeep Upadhaya
                        </h4>
                        <small>
                            K.T.M Immigration, Putalisadak, Kathmandu
                        </small>
                    </div>
                </div>
                                <div>
                    <div class="testimonial-content" style="padding:10px;">
                            <img class="img-responsive" height="65" src="https://ktmimmigration.com.np/uploads/testimonialImg/1502850528-92689153.jpg" width="65">
                            </img>
                      
                       
                            <p>
                                <h3 style="margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span style="font-weight: normal;"><span style="font-size: 11px;">After
taking my Nursing Licensing Examination, I was searching for a reliable
consultancy to pursue my IELTS Examination. Then one of my friend who
previously took IELTS class recommended me Ktm immigration. When I came to ktm
immigration for taking IELETS class,I found that environment here is very
peaceful for study and teachers and every staff member here are very friendly
and helpful.Here,they conduct weekly mock test and provide students with latest
educational material which is very helpful for scoring good in real IELETS
examination.In my opinion,it is one of the best consultancy for taking IELTS
class.</span><span style="font-size: 16pt;"><o:p></o:p></span></span></h3>
                            </p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>
                            Asmita Thapa Magar
                        </h4>
                        <small>
                            K.T.M Immigration, Putalisadak, Kathmandu
                        </small>
                    </div>
                </div>
                                <div>
                    <div class="testimonial-content" style="padding:10px;">
                            <img class="img-responsive" height="65" src="https://ktmimmigration.com.np/uploads/testimonialImg/1502850625-81102391.jpg" width="65">
                            </img>
                      
                       
                            <p>
                                <h3 style="margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span style="font-weight: normal;"><span style="font-size: 11px;">with
various learning materials.It has friendly environment.Teachers are very
cooperative,supportive and flexible.I am glad to be the part of ktmimmigration.</span><span style="font-size: 16pt;"><o:p></o:p></span></span></h3>
                            </p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>
                            Karuna Aryal
                        </h4>
                        <small>
                            K.T.M Immigration, Putalisadak, Kathmandu
                        </small>
                    </div>
                </div>
                                <div>
                    <div class="testimonial-content" style="padding:10px;">
                            <img class="img-responsive" height="65" src="https://ktmimmigration.com.np/uploads/testimonialImg" width="65">
                            </img>
                      
                       
                            <p>
                                <p class="MsoNormal"><span style="font-size: 11px;">Ktm Immigration is the
consultancy where I have been studying IELTS. I found this consultancy cheaper
and better also. It has also good environment. I also recommend other people to
take IELTS or others classes.</span><span style="font-size: 16pt;"><o:p></o:p></span></p>
                            </p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>
                            Anamika Gurung
                        </h4>
                        <small>
                            K.T.M Immigration, Putalisadak, Kathmandu
                        </small>
                    </div>
                </div>
                            </div>
        </div>
        <div class="col-md-4 col-lg-4 " >
            <div class="hgroup">
                <h1 class="text-center">
                    Notice Board
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                                                                                                <ul class="notice-board">
                                        <marquee direction="up" height="390">
                                                                                        <li>
                                                <h1>
                                                    3rd FEBRUARY 2017
                                                </h1>
                                                <p>
                                                    ALL THE STUDENTS
OF IELTS AND JAPANESE LANGUAGE ARE ADVISED TO TAKE THEIR ATTTENDENCE SERIOUSLY.
THERE WILL BE AN ATTENDENCE SYSTEM IMPLEMENTING FROM 3rd FEBRAUARY
2017. THUS, WE REQUEST YOU TO HAVE MORE THAN 80% OF THE WHOLE ATTENDENCE FOR
FURTHER VISA PROCESS.

Thank You!
                                                </p>
                                            </li>
                                                                                        <li>
                                                <h1>
                                                    4th FEBRUARY 2017
                                                </h1>
                                                <p>
                                                    1.     
MORNING
CLASS FOR IELTS WILL RESUME FROM 5th February 2017 ONWARDS. ALL THE
IELTS STUDENTS ARE REQUESTED TO ATTEND THEIR CLASSES REGULARLY.

2.     
THOSE
WHO WANTS TO BOOK THE IELTS DATE ARE REQUESTED TO BRING THEIR PHOTOCOPY OF THE
PASSPORT ALONG WITH THE REQUIRED MONEY.
                                                </p>
                                            </li>
                                                                                    </marquee>
                                    </ul>
                                                                                            </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
        <div class="col-md-4 col-lg-4 " >
            <div class="hgroup">
                <h1>
                    WHY CHOOSE US
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="why-choose-us">
                                <marquee direction="up" height="390">
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            We guide your gateway
                                            <p>
                                                As defined by our motto
"We guide your gateway", we aim to guide our students to effectively
concentrate on their dreams thereby us providing accurate relevant information’s,
helping them make useful efforts to change those dreams into reality.
                                            </p>
                                        </img>
                                    </li>
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            Here is how we help you
                                            <p>
                                                         We provide you a FREE Counseling
session (Telephonic/In-person/online) to help you choose a right
college/university and industry-oriented course in Australia.
                                            </p>
                                        </img>
                                    </li>
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            Tracking Application
                                            <p>
                                                We guide you through the process of
applying for offer letter and we take the responsibility of tracking the
applications by staying in touch with the institution on your behalf.
                                            </p>
                                        </img>
                                    </li>
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            Lodge visa
                                            <p>
                                                Just like your admission application,
KTM Immigration helps you lodge your visa file. We will keep you updated on the
progress of your application and will track your application on your behalf.
                                            </p>
                                        </img>
                                    </li>
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            Document arrangement 
                                            <p>
                                                We help you arrange all your documents properly
as instructed in student visa checklist.
                                            </p>
                                        </img>
                                    </li>
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            Tracking Application
                                            <p>
                                                 We will notify you as soon as we get
to know the outcome of your visa application.
                                            </p>
                                        </img>
                                    </li>
                                                                                                                                                                                                                                        <li class="news-item">
                                        <img src="https://ktmimmigration.com.np/uploads/whyChooseUs">
                                            Service after visa
                                            <p>
                                                   At KTM Immigration, we can help you
organize your travel arrangements to begin your new student life in Australia.
Air ticketing, insurance and foreign exchange can all be arranged through us to
make studying overseas easier and a once-in-a-life experience.
                                            </p>
                                        </img>
                                    </li>
                                                                    </marquee>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
    </div>
</div>
<div class="container block-content">
    <div class="row">
        <div class="col-md-4 col-lg-4 " >
            <div class="hgroup">
                <h1 class="text-center">
                    Latest News
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul style="padding-left: 0px; list-style: none;">
                                <marquee direction="up" height="390">
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/MQ==">
                                                        <h1>
                                                            नेपालीलाई अष्ट्रेलियाको रेडियोमा छात्रवृत्ति
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/Mg==">
                                                        <h1>
                                                            नेपालका बाढीपीडितको सहयोगार्थ अष्ट्रेलियामा राहत सहयोग संकलन शुरु
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/Mw==">
                                                        <h1>
                                                            अष्ट्रेलियामा डा. के.सीको समर्थनमा ऐक्यबद्धता भेला हुने
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/NQ==">
                                                        <h1>
                                                            अष्ट्रेलियाको पीआर अष्ट्रेलियन नागरिकता सरह हुन्छ ?
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/Ng==">
                                                        <h1>
                                                            अष्ट्रेलियाको सिड्नीमा भव्यताका साथ मनाईयो तीज
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/Nw==">
                                                        <h1>
                                                            Australia may introduce mandatory provisional visas before permanent residency
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="https://ktmimmigration.com.np/viewNews/OA==">
                                                        <h1>
                                                            Your chance to migrate to Australia. New occupation list for 2017-18 announced
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                    </marquee>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
        <div class="col-md-4 col-lg-4 " >
            <div class="hgroup">
                <h1 class="text-center">
                    Recent Visa Success
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="demo1" style="padding-left: 0px; list-style: none;">
                                <marquee direction="up" height="390">
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502811453-11649482.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Subash Puri
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502811322-88727122.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Sabina Shrestha
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502811911-63173057.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Saroj Sunuwar
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812021-32208083.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Kanantu Newar
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812119-19207528.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Pradeep Khadka
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812181-77029046.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Ramesh Oli
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812235-18286658.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Sanjeev Giri
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812334-41283118.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Amendra Chaudhary
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812429-30304196.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Balram Chaudhary
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812486-71130175.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Dev Khadka
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502813408-28735692.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Bishal Dukuchhu
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812649-32235048.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Ganesh Adhikari
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Japan
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502812778-48198387.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Alisha Chauhan Chhetri
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Australia
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502813170-10436599.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Abeejeet Sapkota
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Norway
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                        <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="https://ktmimmigration.com.np/uploads/visaSuccess/1502813329-64618922.jpg"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            Monika Shrestha
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : Norway
                                                    </p>
                                                                                                                                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                                                    </marquee>
                            </ul>
                                      <div class="text-center">
                                    <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/visasuccesslist">
                                        View
                                            all
                                    </a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
        <div class="col-md-4 col-lg-4 " >
            <div class="hgroup">
                <h1>
                    Recent Test Score
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="recent-score">
                                <marquee direction="up" height="363">
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848000-2274507.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Asmita K.C
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 7.5
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848070-76235478.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Bishal K.C
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 7.5
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848113-41054744.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Urmila Budhathoki
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 6.5
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848157-84518349.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Alisha Chauhan Chhetri
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 6.5
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848201-13173867.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Anisha Shrestha
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 6
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848251-43584176.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Asmita Thapa Magar
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 6.5
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                        <li class="news-item visa-success">
                                        <img class="img-responsive" src="https://ktmimmigration.com.np/uploads/recentScore/1502848364-86594305.jpg">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : Karuna Aryal
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : IELTS
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : 6.5
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                                                    </marquee>
                                <div class="text-center">
                                    <a class="btn btn-default btn-sm" href="https://ktmimmigration.com.np/recentTestScoreDetails">
                                        View
                                            all
                                    </a>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
    </div>
</div>
<div class="container-rows-fluid">
    <div class="container partners block-content">
        <div class="title-space " >
            <div class="hgroup">
                <h1>
                    WE ARE MEMBER OF
                </h1>
            </div>
        </div>
        <div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-min450="2" data-min600="2" data-min768="4" data-navigation="true" data-pagination="false" data-stop-on-hover="true" id="partners">
                    </div>
    </div>
</div>
<div class="container partners block-content">
    <div class="title-space " >
        <div class="hgroup">
            <h1>LIST OF UNIVERSITY</h1>
        </div>
    </div>
    <div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-min450="2" data-min600="2" data-min768="4" data-navigation="true" data-pagination="false" data-stop-on-hover="true" id="partners">
                <div class="" >
            <a href="https://ktmimmigration.com.np/singleUniDetails/MzA=">
                <img alt="Img" src="https://ktmimmigration.com.np/uploads/unilogo"/>
            </a>
            <h6>
                abc
            </h6>
        </div>
                <div class="" >
            <a href="https://ktmimmigration.com.np/singleUniDetails/MTM=">
                <img alt="Img" src="https://ktmimmigration.com.np/uploads/unilogo/1490769856-66877654.png"/>
            </a>
            <h6>
                CQ University
            </h6>
        </div>
                <div class="" >
            <a href="https://ktmimmigration.com.np/singleUniDetails/OA==">
                <img alt="Img" src="https://ktmimmigration.com.np/uploads/unilogo/1490345994-68026434.jpg"/>
            </a>
            <h6>
                latrobe university
            </h6>
        </div>
                <div class="" >
            <a href="https://ktmimmigration.com.np/singleUniDetails/OQ==">
                <img alt="Img" src="https://ktmimmigration.com.np/uploads/unilogo/1490346037-37207449.jpg"/>
            </a>
            <h6>
                oxford 
            </h6>
        </div>
                <div class="" >
            <a href="https://ktmimmigration.com.np/singleUniDetails/Nw==">
                <img alt="Img" src="https://ktmimmigration.com.np/uploads/unilogo/1490345932-9116131.png"/>
            </a>
            <h6>
                Southern University
            </h6>
        </div>
                <div class="" >
            <a href="https://ktmimmigration.com.np/singleUniDetails/Ng==">
                <img alt="Img" src="https://ktmimmigration.com.np/uploads/unilogo/1490344491-10628164.jpg"/>
            </a>
            <h6>
                Victoria University
            </h6>
        </div>
            </div>
</div>
<footer>
    <div class="color-part2"></div>
    <div class="color-part"></div>
    <div class="container">
        <div class="row block-content">
            <div class="col-md-4">
                <div class="footerlogo">
                    <img src="../frontend/default/img/logo.png" class="img-responsive">
                </div>
                <p>We provide a wide range of services that guide students to effectively concentrate on their
                    dreams thereby us making the efforts to change those dreams into reality.</p>
                <div class="footer-icons">
                    <a target="_blank" href="https://www.facebook.com/KTMIMMIGRATION/"><i
                                class="fa fa-facebook-square fa-2x"></i></a>
                    <a target="_blank" href="https://plus.google.com/u/1/108551602522247848911"><i
                                class="fa fa-google-plus-square fa-2x"></i></a>
                    <a target="_blank" href="https://twitter.com/KtmImmigration"><i
                                class="fa fa-twitter-square fa-2x"></i></a>
                    <a target="_blank" href="https://www.youtube.com/channel/UCRZ-WtDYA9WcT26LzHM3-TQ"><i
                                class="fa fa-youtube fa-2x"></i></a>
                </div>
                <a href="https://ktmimmigration.com.np/applyOnlineNow" class="btn btn-danger">APPLY ONLINE NOW</a>
            </div>
            <div class="col-md-3">
                <h4>Follow us on social media</h4>
                <div class="fb-page" data-href="https://www.facebook.com/KTMIMMIGRATION/" data-tabs="timeline"
                     data-width="193" data-height="233" data-small-header="true" data-adapt-container-width="false"
                     data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/KTMIMMIGRATION/" class="fb-xfbml-parse-ignore"><a
                                href="https://www.facebook.com/KTMIMMIGRATION/">KTM Immigration</a></blockquote>
                </div>
            </div>
            <div class="col-md-2 ">
                <h4>MAIN LINKS</h4>
                <nav>
                    <a href="index.php">HOME</a>
                    <a href="https://ktmimmigration.com.np/about-us">ABOUT US</a>
                    <a href="https://ktmimmigration.com.np/allservices">OUR SERVICES</a>
                    <a href="https://ktmimmigration.com.np/countriesList">COUNTRY INFORMATION</a>
                    <a href="https://ktmimmigration.com.np/universityList">UNIVERSITY INFORMATION</a>
                    <a href="https://ktmimmigration.com.np/blog">BLOG</a>
                </nav>
            </div>
            <div class="col-md-3 ">
                <h4>CONTACT INFO</h4>
                <div class="contact-info">
                    <span><i class="fa fa-location-arrow"></i><strong>KTM IMMIGRATION PVT LTD.</strong><br>Putalisadak, Kathmandu, Nepal</span>
                    <span><i class="fa fa-phone"></i>+977-01-4426263</span>
                    <span><i class="fa fa-envelope"></i>immigrationktm@gmail.com </span>
                    <span><i class="fa fa-globe"></i>www.ktmimmigration.com.np</span>
                    <span><i class="fa fa-clock-o"></i>Sun - Fri  10.00 - 5.00</span>
                </div>
            </div>
        </div>
        <div class="copy text-right">
            <a style="" href="https://ktmimmigration.com.np/privacypolicy">Privacy policy</a>
            <a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
        Created by <a target="_blank" href="www.dbasofttech.com">DBASoftTech</a> &copy; 2017 KTM IMMIGRATION All
            rights
            reserved.
        </div>
    </div>
</footer>


<!-- Javascript -->
<!--Main-->
<script src="https://ktmimmigration.com.np/frontend/default/js/jquery-1.11.3.min.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/js/jquery-ui.min.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/js/bootstrap.min.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/js/modernizr.custom.js"></script>
<!--scroller -->

<!-- Loader -->
<script src="https://ktmimmigration.com.np/frontend/default/assets/loader/js/classie.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/assets/loader/js/pathLoader.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/assets/loader/js/main.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/js/classie.js"></script>
<!--chosen-->
<script src="https://ktmimmigration.com.np/frontend/default/js/chosen.jquery.min.js"></script>
<!--Switcher-->
<script src="https://ktmimmigration.com.np/frontend/default/assets/switcher/js/switcher.js"></script>
<!--Owl Carousel-->
<script src="https://ktmimmigration.com.np/frontend/default/assets/owl-carousel/owl.carousel.min.js"></script>
<!--Contact form-->
<!-- <script src="https://ktmimmigration.com.np/frontend/default/assets/contact/jqBootstrapValidation.js"></script>
            <script src="https://ktmimmigration.com.np/frontend/default/assets/contact/contact_me.js"></script>
      -->       <!-- SCRIPTS -->
<script type="text/javascript" src="https://ktmimmigration.com.np/frontend/default/assets/isotope/jquery.isotope.min.js"></script>
<!--Theme-->
<script src="https://ktmimmigration.com.np/frontend/default/js/jquery.smooth-scroll.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/js/jquery.placeholder.min.js"></script>
<script src="https://ktmimmigration.com.np/frontend/default/js/theme.js"></script>
<script src="https://ktmimmigration.com.np/sximo/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://ktmimmigration.com.np/sximo/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.selectpicker').chosen({width: "100%"});
        $('.date').datepicker({format: 'yyyy-mm-dd', autoClose: true})
        $('.time').datetimepicker({format: 'hh:ii:ss'});
    });
    var stickyNav = function () {
        var game = $(window).width();
        if (game <= 798) {
            $("#topNav").removeClass("blockDisplay");
            $("ul.navbar-main > li").removeClass("displaynone");
            $(window).scroll(function () {
                if ($(window).scrollTop() > 400) {
                    $('#menu-open').addClass('stuck');
                } else {
                    $('#menu-open').removeClass('stuck');
                }
            });
        }
    }
    stickyNav();
    $(window).resize(function () {
        stickyNav();
    });
</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#techSupport').click(function (e) {
            e.preventDefault();
            $zopim.livechat.window.show();
        });

        $('#askTheExpertClose').click(function (e) {
            e.preventDefault();
            $('#livechat').hide();
            $zopim.livechat.window.hide();
        })
    });
</script>
<!--End of Zendesk Chat Script-->

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".searchDestination #click_btn").click(function () {
            var select_field = jQuery(".searchDestination #validation").val();
            if (select_field === "") {
                jQuery(".searchDestination #validation").addClass("validation-color");
                jQuery(".error-msg").html("All three Field Required");
                return false;
            }

        });

        jQuery("#click_top_search").click(function () {
            var select_field_top = jQuery(".search-form-modal #keywordsearch").val();
            if (select_field_top === "") {
                alert("Search Input Field Required For Search");
                jQuery("#keywordsearch").addClass("validation-color");
                return false;
            }

        });
    });


    /**
     * @param    {[dependent visa]}
     * @return  {[dependent visa]}
     */
    jQuery(document).ready(function ($) {
        jQuery('[data-toggle="tooltip"]').tooltip();
        jQuery("#valueyesdependable").click(function () {
            if (jQuery("#valueyesdependable").val() == "Yes") {
                jQuery(".itemyes a").attr('data-toggle', 'tab');
                jQuery(".itemyes a").removeClass('disabled');
                jQuery(".deactiveated").hide();
                jQuery("html, body").animate({scrollTop: 0}, "slow");
                jQuery(".itemyes").removeAttr("data-original-title", "tooltip");
            }
        });
    });

    jQuery(document).ready(function () {
        jQuery(".itemyes").click(function () {
            jQuery(".deactiveated").show();
        });
    })
</script>
</body>
</html>
